package main


type Todo struct {
	Id        int       `json:"id"`
	Username string `json:"username"`
	Psswd string `json:"psswd"`
	Mobile string `json:"mobile"`
	Email string `json:"email"`
	Contacts struct {
		Id int `json:"id"`
		Messages struct {
			Id int `json:"id"`
			BodyMessage string `json:"bodyMessage"`
		} `json:"messages"`
	} `json:"contacts"`
}

type Todos []Todo
