//
//  AddContactViewController.m
//  Secured Messenger
//
//  Created by Alexandre ERNY on 22/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import "AddContactViewController.h"
#import "User.h"
#import "MessageViewController.h"
#import "ApplicationSingleton.h"

@interface AddContactViewController ()

@end

@implementation AddContactViewController

@synthesize mailAddress, phoneNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [self.mailAddress resignFirstResponder];
    [self.phoneNumber resignFirstResponder];
}


#pragma mark Create contact functions

- (IBAction)createContact:(id)sender
{
    User *newContact = [[User alloc] init];
    newContact.username = @"toto";
    if (![mailAddress.text isEqualToString:@""])
    {
        newContact.email = mailAddress.text;
    }
    else if (![phoneNumber.text isEqualToString:@""])
    {
        newContact.phone = phoneNumber.text;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter email address or phone number"
                                                        message:@"You must enter either the email address or the phone number of your contact."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return ;
    }
    [[[ApplicationSingleton sharedInstance] objectManager] postObject:newContact path:@"/addContact" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
     {
         
         [self.navigationController popViewControllerAnimated:YES];
     }
                                                              failure:^(RKObjectRequestOperation *operation, NSError *error)
     {
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact not found"
                                                         message:@"Please check the entered email address/phone number and your internet connexion !"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
         [alert show];
         
         [self.navigationController popViewControllerAnimated:YES];
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
