//
//  SubscribeController.m
//  Secured Messenger
//
//  Created by Alexandre ERNY on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <RestKit.h>
#import "User.h"
#import "SubscribeController.h"
#import "ApplicationSingleton.h"

@interface SubscribeController ()
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *mailAddress;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;

@end

@implementation SubscribeController

@synthesize username, password, mailAddress, phoneNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (IBAction)subscribe:(id)sender {
    User *newUser = [[User alloc] init];
    newUser.username = username.text;
    newUser.password = password.text;
    newUser.email = mailAddress.text;
    newUser.phone = phoneNumber.text;
        [[[ApplicationSingleton sharedInstance] objectManager] postObject:newUser path:@"/users" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Subscription Complete !"
                                                        message:@"You can now connect to Secured Messenger !"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Subscription Error !"
                                                        message:@"We could not register your account ! Please check your network connection and try changing your requested username/e-mail address."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [self.nameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.phoneNumberTextField resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
