//
//  MessageListTableViewCell.h
//  Secured Messenger
//
//  Created by Diana on 21/02/2015.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end
