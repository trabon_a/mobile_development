//
//  ViewController.h
//  Secured messenger
//
//  Created by Alexandre ERNY on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *tableData;

@property (weak, nonatomic) IBOutlet UITableView *messsagesTableView;

- (void)refreshData;

@end

