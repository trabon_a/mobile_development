//
//  MessageViewController.m
//  Secured messenger
//
//  Created by Diana Nejdi on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import "MessageViewController.h"
#import "ApplicationSingleton.h"
#import "User.h"
#import "Message.h"

@interface MessageViewController ()

@end

@implementation MessageViewController

@synthesize contact;

- (void)goToKeysOptions
{
    [self performSegueWithIdentifier:@"keysManagement" sender:self];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.messagesData = [[NSMutableArray alloc] init];
    [[[ApplicationSingleton sharedInstance] objectManager] getObjectsAtPath:[NSString stringWithFormat:@"/messages/%@", contact.id] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        for (Message *msg in mappingResult.array)
        {
            NSData *cryptedData =[[NSData alloc] initWithBase64EncodedString:msg.bodyMessage options:0];
            msg.bodyMessage = [[NSString alloc] initWithData:[[[ApplicationSingleton sharedInstance] pgp] decryptData:cryptedData] encoding:NSUTF8StringEncoding];
            if (msg.bodyMessage == nil || [msg.bodyMessage isEqualToString:@""])
                msg.bodyMessage = @"<Encrypted message>";
            [self.messagesData addObject:msg];
        }
        [self.messsagesTableView reloadData];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    }];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Keys"] style:UIBarButtonItemStylePlain target:self action:@selector(goToKeysOptions)]];
    self.title = contact.username;
    if (![[[ApplicationSingleton sharedInstance] pgp] exportKeyNamed:contact.username])
    {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No public key for this contact"
                                                            message:@"You don't have the public key of this contact yet. You won't be able to send any messages until you scan his public key using the key button on the top right corner of your screen."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
    }
    [[[ApplicationSingleton sharedInstance] pgp] setUserId:contact.username];
    [self checkPublicKey];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardDidHideNotification"
                                               object:nil];    self.messsagesTableView.transform = CGAffineTransformMakeRotation(-M_PI);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)checkPublicKey
{
    if (![[[ApplicationSingleton sharedInstance] pgp] exportKeyNamed:contact.username])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No public key for this contact"
                                                        message:@"You don't have the public key of this contact yet. You won't be able to send any messages until you scan his public key using the key button on the top right corner of your screen."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}

- (IBAction)sendButtonTouched:(id)sender
{
    if (![self checkPublicKey])
        return ;
    NSString *message = self.messageField.text;
    NSData *textcrypted = [[[ApplicationSingleton sharedInstance] pgp] encryptData:[message dataUsingEncoding:NSUTF8StringEncoding] options:UNEncryptOptionNone];
    NSString *readableCrypted = [textcrypted base64EncodedStringWithOptions:0];
    Message *msg = [[Message alloc] init];
    msg.bodyMessage = readableCrypted;
    [msg setId:[[[ApplicationSingleton sharedInstance] loggedUserObject] id]];
    [msg setUserTo_id:[contact id]];
    [[[ApplicationSingleton sharedInstance] objectManager] postObject:msg path:@"/messages" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [self.messagesData addObject:msg];
        self.messageField.text = @"";
        [self.messsagesTableView reloadData];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not send message"
                                                        message:@"Message could not be sent. Please check your internet connexion !"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];

}
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.messagesData count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [[self.messagesData objectAtIndex:[self.messagesData count] - 1 - indexPath.row] bodyMessage];
    
    // changer avec le createur du message
    if ([[[self.messagesData objectAtIndex:[self.messagesData count] - 1 - indexPath.row] id] intValue] == [[contact id] intValue])
    {
        cell.textAlignment = NSTextAlignmentRight;
        cell.textLabel.textColor = [UIColor colorWithRed:(0/255.0) green:(128/255.0) blue:(255/255.0) alpha:1.f];
    }
    else
    {
        cell.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor colorWithRed:(0/255.0) green:(153/255.0) blue:(76/255.0) alpha:1.f];
    }
    
    cell.font = [UIFont fontWithName:@"TrebuchetMS" size:14.f];
    
    cell.transform = CGAffineTransformMakeRotation(M_PI);
    
    return cell;
}


#pragma mark Move view with keyboard

- (void) keyboardWillShow:(NSNotification *)note
{
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
    
    // move the view up by 30 pts
    CGRect frame = self.view.frame;
    frame.origin.y = frame.origin.y - kbSize.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    }];
}

- (void) keyboardDidHide:(NSNotification *)note
{
    // move the view back to the origin
    CGRect frame = self.view.frame;
    frame.origin.y = 0;


    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    }];
}

@end
