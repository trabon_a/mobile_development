//
//  User.h
//  Secured Messenger
//
//  Created by Alexandre ERNY on 22/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSNumber *id;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phone;


@end
