//
//  KeysControllerViewController.m
//  Secured Messenger
//
//  Created by Alexandre ERNY on 22/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "ApplicationSingleton.h"
#import "KeysControllerViewController.h"

@interface KeysControllerViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *QRCode;
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switcher;

@end

@implementation KeysControllerViewController

@synthesize QRCode, cameraView, captureSession, videoPreviewLayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *username = [[ApplicationSingleton sharedInstance] loggedUser];
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setValue:[[[NSData dataWithContentsOfFile:[[[ApplicationSingleton sharedInstance] pgp].homeDirectory stringByAppendingFormat:@"/%@.gpg", username]] base64EncodedStringWithOptions:0] dataUsingEncoding:NSISOLatin1StringEncoding] forKey:@"inputMessage"];
    UIImage *image = [UIImage imageWithCIImage:filter.outputImage];
    QRCode.image = image;
}

- (IBAction)switchMode:(id)sender {
    if (QRCode.isHidden)
    {
        [captureSession stopRunning];
        captureSession = nil;
        videoPreviewLayer = nil;
        [QRCode setHidden:NO];
        [cameraView setHidden:YES];
        
    }
    else
    {
        [QRCode setHidden:YES];
        [cameraView setHidden:NO];
        NSError *error;
        AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
        
        if (!input) {
            NSLog(@"%@", [error localizedDescription]);
            return ;
        }
        captureSession = [[AVCaptureSession alloc] init];
        [captureSession addInput:input];
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [captureSession addOutput:captureMetadataOutput];
        dispatch_queue_t dispatchQueue;
        dispatchQueue = dispatch_queue_create("myQueue", NULL);
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
        [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
        videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:captureSession];
        [videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [videoPreviewLayer setFrame:cameraView.layer.bounds];
        [cameraView.layer addSublayer:videoPreviewLayer];
        [captureSession startRunning];

    }
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (!captureSession.isRunning){ return ;}
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            NSString *data = [metadataObj stringValue];
            NSData *binary = [[NSData alloc] initWithBase64EncodedString:data options:0];
            NSString *filename = [NSString stringWithFormat:@"%@_%@", [[NSProcessInfo processInfo] globallyUniqueString], @".gpg"];
            NSURL *targetURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:filename]];
            [binary writeToURL:targetURL atomically:YES];
            [[[ApplicationSingleton sharedInstance] pgp] importPublicKeyFromFileAtPath:targetURL.path];
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtURL:targetURL error: &error];
            [captureSession stopRunning];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.switcher setSelectedSegmentIndex:0];
                [self switchMode:self.switcher];
            });
            return ;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
