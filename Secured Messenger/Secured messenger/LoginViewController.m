//
//  LoginViewController.m
//  Secured messenger
//
//  Created by Diana Nejdi on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import "LoginViewController.h"
#import "User.h"
#import "ApplicationSingleton.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end

@implementation LoginViewController

@synthesize username, password;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)requestLogin:(id)sender {
    User *userObject = [[User alloc] init];
    userObject.username = username.text;
    userObject.password = password.text;
    userObject.id = 0;
    [username setEnabled:NO];
    [password setEnabled:NO];
    [sender setEnabled:NO];
    [[[ApplicationSingleton sharedInstance] objectManager] postObject:userObject path:@"/login" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [[ApplicationSingleton sharedInstance] initPGPwithusername:userObject.username andPassword:userObject.password];
        [[ApplicationSingleton sharedInstance] setLoggedUserObject:mappingResult.firstObject];
        [self performSegueWithIdentifier:@"loginSuccess" sender:self];
        [username setEnabled:YES];
        [password setEnabled:YES];
        [sender setEnabled:YES];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connexion Failed !"
                                                        message:@"Please check your internet connection and your credentials !"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [username setEnabled:YES];
        [password setEnabled:YES];
        [sender setEnabled:YES];

    }];
}

-(void)dismissKeyboard {
    [self.nameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

@end
