//
//  ContactViewController.m
//  Secured messenger
//
//  Created by Diana Nejdi on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import "ContactViewController.h"
#import "MessageViewController.h"
#import "ViewController.h"
@interface ContactViewController ()

@property (weak, nonatomic) UITableView *tableview;



@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.searchResults = [[NSArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return [self.searchResults count];
    else
        return [self.contactsData count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
        cell.textLabel.text = [[self.searchResults objectAtIndex:indexPath.row] username];
    else
        cell.textLabel.text = [[self.contactsData objectAtIndex:indexPath.row] username];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _tableview = tableView;
    [self performSegueWithIdentifier:@"showMessages" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showMessages"])
    {
        MessageViewController *controller = [segue destinationViewController];
        if (_tableview == self.searchDisplayController.searchResultsTableView)
            controller.contact = self.searchResults[_tableview.indexPathForSelectedRow.item];
        else
            controller.contact = self.contactsData[_tableview.indexPathForSelectedRow.item];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [_vc refreshData];
    [_tableview reloadData];
};

#pragma mark - Navigation



# pragma Search bar methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"username contains[c] %@", searchText];

    self.searchResults = [self.contactsData filteredArrayUsingPredicate:resultPredicate];

}

-(BOOL)searchDisplayController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
