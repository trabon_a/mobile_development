//
//  ViewController.m
//  Secured messenger
//
//  Created by Alexandre ERNY on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import "MessageViewController.h"
#import "MessageListTableViewCell.h"
#import "ViewController.h"
#import "ApplicationSingleton.h"
#import "ContactViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)refreshData
{
    [[[ApplicationSingleton sharedInstance] objectManager] getObjectsAtPath:@"/contacts" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        self.tableData = mappingResult.array;
        [self.tableView reloadData];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not retrieve contact list !"
                                                        message:@"Could not retrieve contact list, please login again."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];

}


- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self refreshData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"MessageListTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    
    cell.nameLabel.text = [[self.tableData objectAtIndex:indexPath.row] username];
    cell.messageLabel.text = [[self.tableData objectAtIndex:indexPath.row] email];
    
    return cell;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goMessages"])
    {
        MessageViewController *controller = [segue destinationViewController];
        controller.contact = self.tableData[_tableView.indexPathForSelectedRow.item];
    }
    else //Segue searchContact
    {
        ContactViewController *controller = [segue destinationViewController];
        controller.contactsData = self.tableData;
        controller.vc = self;
    }
}


#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    MessageViewController *vc = [[MessageViewController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
    [self performSegueWithIdentifier:@"goMessages" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
