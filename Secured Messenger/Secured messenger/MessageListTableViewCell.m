//
//  MessageListTableViewCell.m
//  Secured Messenger
//
//  Created by Diana on 21/02/2015.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import "MessageListTableViewCell.h"

@implementation MessageListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
