//
//  Message.h
//  Secured Messenger
//
//  Created by Alexandre ERNY on 22/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (strong, nonatomic) NSString *bodyMessage;
@property (strong, nonatomic) NSNumber *id;
@property (strong, nonatomic) NSNumber *userTo_id;

@end
