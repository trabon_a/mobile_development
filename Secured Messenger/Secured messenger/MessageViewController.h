//
//  MessageViewController.h
//  Secured messenger
//
//  Created by Diana Nejdi on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface MessageViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *messagesData;

@property (weak, nonatomic) IBOutlet UITableView *messsagesTableView;

@property (strong, nonatomic) User *contact;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UITextField *messageField;

@end

