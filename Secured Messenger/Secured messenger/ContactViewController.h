//
//  ContactViewController.h
//  Secured messenger
//
//  Created by Diana Nejdi on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "ViewController.h"

@interface ContactViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchControllerDelegate>

@property (strong, nonatomic) NSArray *contactsData;
@property (strong, nonatomic) NSArray *searchResults;
@property (weak, nonatomic) ViewController *vc;

@property (weak, nonatomic) IBOutlet UITableView *messsagesTableView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

