//
//  ApplicationSingleton.h
//  Secured Messenger
//
//  Created by Alexandre ERNY on 22/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit.h>
#import <UNNetPGP.h>
#import "User.h"

@interface ApplicationSingleton : NSObject

@property (strong, nonatomic) NSUserDefaults *userParameters;
@property (strong, nonatomic) RKObjectManager *objectManager;
@property (strong, nonatomic) UNNetPGP *pgp;
@property (strong, nonatomic) NSString *loggedUser;
@property (strong, nonatomic) User *loggedUserObject;

+ (id)sharedInstance;
- (void)initPGPwithusername: (NSString *) username andPassword: (NSString *) password;

@end
