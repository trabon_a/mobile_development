//
//  LoginViewController.h
//  Secured messenger
//
//  Created by Diana Nejdi on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

