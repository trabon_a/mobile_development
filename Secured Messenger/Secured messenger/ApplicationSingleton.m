//
//  ApplicationSingleton.m
//  Secured Messenger
//
//  Created by Alexandre ERNY on 22/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import "ApplicationSingleton.h"
#import "User.h"
#import "Message.h"

@implementation ApplicationSingleton

@synthesize userParameters, objectManager, pgp, loggedUser;

+ (id)sharedInstance
{
    static ApplicationSingleton *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)initRestKit
{
    objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"http://noraj.fr:5000"]];
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    
    // User object
    
    RKObjectMapping *userRequestMapping = [RKObjectMapping requestMapping];
    RKObjectMapping *userResponseMapping = [RKObjectMapping mappingForClass:[User class]];
    [userRequestMapping addAttributeMappingsFromArray:@[ @"username" , @"password", @"email", @"phone", @"id"]];
    [userResponseMapping addAttributeMappingsFromArray:@[ @"username" , @"password", @"email", @"phone", @"id"]];
    
    // User response object
    
    RKResponseDescriptor *userResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userResponseMapping method:RKRequestMethodAny pathPattern:@"/contacts" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:userResponseDescriptor];
    RKResponseDescriptor *userloginResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userResponseMapping method:RKRequestMethodAny pathPattern:@"/login" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:userResponseDescriptor];
    [objectManager addResponseDescriptor:userloginResponseDescriptor];
    
    // User request object
    
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:userRequestMapping objectClass:[User class] rootKeyPath:nil method:RKRequestMethodPOST];
    [objectManager addRequestDescriptor:requestDescriptor];
    [objectManager setRequestSerializationMIMEType:RKMIMETypeJSON];
    
    // Message object
    
    RKObjectMapping *messageResponseMapping = [RKObjectMapping mappingForClass:[Message class]];
    RKObjectMapping *messageRequestMapping = [RKObjectMapping requestMapping];
    [messageResponseMapping addAttributeMappingsFromArray:@[ @"id", @"bodyMessage", @"userTo_id"]];
    [messageRequestMapping addAttributeMappingsFromArray:@[ @"id", @"bodyMessage", @"userTo_id"]];
    
    // Message response object
    RKResponseDescriptor *messageResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:messageResponseMapping method:RKRequestMethodAny pathPattern:@"/messages/:id" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:messageResponseDescriptor];
    
    
    
    // Message request object
    RKRequestDescriptor *messageRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:messageRequestMapping objectClass:[Message class] rootKeyPath:nil method:RKRequestMethodPOST];
    [objectManager addRequestDescriptor:messageRequestDescriptor];

}


- (void)initPGPwithusername: (NSString *) username andPassword: (NSString *) password
{
    loggedUser = username;
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    pgp = [[UNNetPGP alloc] initWithUserId:username];
    pgp.password = password;
    pgp.armored  = YES;
    
    NSString* keyring = [pgp.homeDirectory stringByAppendingFormat:@"/priv_%@.gpg", username];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:keyring];
    
    pgp.publicKeyRingPath = [pgp.homeDirectory stringByAppendingFormat:@"/pub_%@.gpg", username];
    pgp.secretKeyRingPath = [pgp.homeDirectory stringByAppendingFormat:@"/priv_%@.gpg", username];
    
    if (!fileExists)
    {
        [pgp generateKey:2048];
        NSError *error;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager copyItemAtPath:[pgp.homeDirectory stringByAppendingFormat:@"/pub_%@.gpg", username] toPath:[pgp.homeDirectory stringByAppendingFormat:@"/%@.gpg", username] error:&error];
    }
    NSLog(@"%@", [pgp availableKeys]);
}

- (id)init {
    if (self = [super init])
    {
        userParameters = [NSUserDefaults standardUserDefaults];
        [self initRestKit];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
