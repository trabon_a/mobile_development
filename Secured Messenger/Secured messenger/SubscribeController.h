//
//  SubscribeController.h
//  Secured Messenger
//
//  Created by Alexandre ERNY on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscribeController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;

@end
