//
//  KeysControllerViewController.h
//  Secured Messenger
//
//  Created by Alexandre ERNY on 22/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeysControllerViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@end
