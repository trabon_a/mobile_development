//
//  main.m
//  Secured messenger
//
//  Created by Alexandre ERNY on 21/02/15.
//  Copyright (c) 2015 epitech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
    